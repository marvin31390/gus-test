import * as React from "react";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import {Link} from 'gatsby-plugin-react-i18next';

const flag = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap",
  marginRight: "10%"
};
const lol = {
  color: "#FFF",
  paddingLeft: "35px",
  marginTop: "40px",
};
const loli = {
  height: "36px",
  width: "54px",
};

const Flags = ({ data }) => {
  const imageFr = getImage(data.allImageSharp.edges[3].node.gatsbyImageData);
  const imageEs = getImage(data.allImageSharp.edges[2].node.gatsbyImageData);
  const imageEn = getImage(data.allImageSharp.edges[1].node.gatsbyImageData);
  const imageKr = getImage(data.allImageSharp.edges[4].node.gatsbyImageData)
  return (
    <div>
      <div style={flag}>
          <div style={lol}>
            <Link to="/" language="fr"><GatsbyImage image={imageFr} alt="drapeau francais" style={loli} />{" "}</Link>
            <p>Français</p>
          </div>
          <div style={lol}>
            <Link to="/" language="en"><GatsbyImage image={imageEn} alt="english flag" style={loli} /></Link>
            <p>English</p>
          </div>
          <div style={lol}>
            <Link to="/" language="es"><GatsbyImage image={imageEs} alt="bandera española" style={loli} /></Link>
            <p>Español</p>
          </div>
          <div style={lol}>
            <Link to="/" language="kr"><GatsbyImage image={imageKr} alt="한국 국기" style={loli} /></Link>
            <p>한국어</p>
          </div>
      </div>
    </div>
  );
};

export default Flags;

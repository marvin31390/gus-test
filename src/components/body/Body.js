import * as React from "react";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import {useTranslation} from 'gatsby-plugin-react-i18next';
import ReactMap from "./ReactMap"
import Tel from "../../assets/tel.svg"
import Info from "../../assets/info.svg"
import MapIcon from "../../assets/map.svg"
import Home from "../../assets/home.svg"
import Health from "../../assets/health.svg"
import Shower from "../../assets/shower.svg"
import Fork from "../../assets/fork.svg"
import Tshirt from "../../assets/tshirt.svg"
import Slumber from "../../assets/slumber.svg"



const body = {
  height: "100%",
};
const textUnder = {
  height: "auto",
  width: "230px",
  marginLeft: "20px",
  paddingBottom: '15px'
};
const slide = {
  width: "50px",
  height: "510px",
  backgroundColor: "#ADACB5",
  position: "absolute",
  marginLeft: "84%",
  top: "260px",
  borderTopLeftRadius: "20px",
  borderBottomLeftRadius: "20px",
};
const iconCategories = {
  width: "40px",
  height: "40px",
  marginLeft: "5px",
  marginTop: "5px",
  backgroundColor: "#FFF",
  borderRadius: "25px",
};
const loli = {
  height: "20px",
  width: "30px",
  marginLeft: "12%",
  marginTop: "22%"
};
const map = {
  height: "505px",
  width: "75%",
  marginLeft: "20px",
  marginTop: "5px",
  borderRadius: "20px",
};
const link = {
  color: "#000"
}
const Body = ({ data }) => {
  const imageFr = getImage(data.allImageSharp.edges[3].node.gatsbyImageData);
  const chevron = getImage(data.allImageSharp.edges[7].node.gatsbyImageData);
  const {t} = useTranslation();
  

  return (
    <div style={body}>
      <div style={map}><ReactMap data={data}/></div>
      <div style={textUnder}>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae est id metus dignissim convallis sed et purus.
      Ut et ultrices sem, ac rutrum lacus. Ut pulvinar odio nisl, vitae pretium augue pellentesque vel. Quisque dictum consequat 
      leo, eget viverra massa facilisis sed. Praesent eget odio ipsum. Praesent tempus tortor in dui commodo tincidunt. Ut aliquam
      rhoncus enim, vel commodo orci. Praesent ultricies luctus dui, at eleifend velit pulvinar et.Proin id metus sagittis, porta
      urna ornare, placerat purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
      Phasellus diam ante, dictum eu sagittis laoreet, tristique quis ante. Nunc et elit vitae risus bibendum pharetra non sit amet
      t. Donec purus lectus, mollis quis justo ut, molestie bibendum odio. Donec et eleifend quam. Proin scelerisque, nisl quis
      convallis vestibulum, orci lacus tempor nulla, eu tristique neque est fringilla nunc. Nulla sit amet tempor enim. Praesent
      ut leo nisi.Duis quis semper sapien. Donec tempus in massa eu tincidunt. Ut eget purus condimentum, sodales erat id, 
      venenatis nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut pharetra nunc, sed facilisis elit.
      Nam tempor lectus tempor iaculis dictum. Vestibulum commodo mi eget dui tristique, et egestas est rutrum.</p>
      </div>
      <div style={slide}>
        <div style={iconCategories}>
        <a href="/categories"><GatsbyImage image={chevron} alt="" style={loli} />{" "}</a>
        </div>
        <div style={iconCategories}>
          <a href="/language"><GatsbyImage image={imageFr} alt="" style={loli} />{" "}</a>
        </div>
        <div style={iconCategories}>
          <a href="/usefullNumber" aria-label={t('numbers')} style={link}><Tel style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#infos" style={link} aria-label={t('welcome')}><Info style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="/map" style={link} aria-label={t('map')}><MapIcon style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#domicile" style={link} aria-label={t('home')}><Home style={loli} /></a>
        </div>
        <div style={iconCategories}>
          <a href="#soigner" style={link} aria-label={t('heal')}><Health style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#laver" style={link} aria-label={t('wash')}><Shower style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#manger" style={link} aria-label={t('eat')}><Fork style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#habiller" style={link} aria-label={t('dress')}><Tshirt style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#hebergement" style={link} aria-label={t('accomodation')}><Slumber style={loli}/></a>
        </div>
      </div>
      
      </div>
    
  );
};

export default Body;

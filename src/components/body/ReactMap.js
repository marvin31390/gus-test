import React from "react";
import PropTypes from "prop-types";
import "../../style/leafletmap.css";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";


class ReactMap extends React.Component {
  
  state = {
    domicile: [],
    habiller: [],
    hebergement: [],
    infos: [],
    laver: [],
    manger: [],
    soigner: []
    
  }

  static propTypes = {
    /** Latitude and Longitude of the map centre in an array, eg [51, -1] **/
    position: PropTypes.array,

    /** Initial zoom level for the map (default 13) **/
    zoom: PropTypes.number,

    /** If set, will display a marker, which when clicked will display this text **/
    markerText: PropTypes.string,
  };

   static defaultProps = {
     position: [43.1074615, 0.7237405],
     zoom: 14,
     markerText: "hello",
   };
  
  componentDidMount(){
   
      this.setState({domicile: this.props.data.allMarkersJson.edges[0].node.domicile});
      this.setState({habiller: this.props.data.allMarkersJson.edges[0].node.habiller});
      this.setState({hebergement: this.props.data.allMarkersJson.edges[0].node.hebergement});
      this.setState({infos: this.props.data.allMarkersJson.edges[0].node.infos});
      this.setState({laver: this.props.data.allMarkersJson.edges[0].node.laver});
      this.setState({manger: this.props.data.allMarkersJson.edges[0].node.manger});
      this.setState({soigner: this.props.data.allMarkersJson.edges[0].node.soigner}); 
      
  }


  render() {
    
    
      return (
        <MapContainer center={this.props.position} zoom={this.props.zoom}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='`&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors`'
          />
          {Object.entries(this.state).map(([key , value])=> 
          {
            if(window.location.hash.endsWith(key) || window.location.hash === '') 
            { 
              return Object.entries(value).map(([keys, values])=>
              {
                return <Marker key={keys} position={[values.Latitude, values.Longitude]}>
                  <Popup>{values.Nom} <br /> {values.Adresse} <br /> {values.Telephone1}</Popup>
          
                  </Marker>
              })

            }
          })}
      
        </MapContainer>
      );
    }
}

export default ReactMap;

import * as React from 'react'




const footer = {
  textAlign: "center",
  backgroundColor: "#2D3142",
  height: "150px",
  width: "auto",
  color: "#FFF",
  fontSize: "1.5em"
}
const footerText = {
paddingTop: "60px"
}

const Footer = () => {

  return (
    <div style={footer}>
        <p style={footerText}>© Simplon Carbonne - 2021 </p>
    </div>
  )
 }

 export default Footer
import * as React from 'react';
import {useTranslation} from 'gatsby-plugin-react-i18next';

//style
const title = {
    backgroundColor: "#FFF",
    color: "#000",
    height: "100px",
    width: "265px",
    textAlign: "center",
    borderRadius: "25px",
    paddingTop: "5px",
    marginTop: "30px",
    marginLeft: "8%",
    fontSize: "1.5em",
    marginBottom: "5%"
}

const titleP = {
    paddingTop: "10px",
    fontSize: "1em",
    fontWeight: "bold",
    margin: "0",
}
const linkStyle = {
  textDecoration: "none",
  color: "#000"
}

const Title = () => {
  const {t} = useTranslation();
  return (
    <div style={title}>
      <a href="/" style={linkStyle}><p style={titleP}>{t('title')} <br />
      {t('subtitle')}</p></a>

    </div>
  )
 }

 export default Title
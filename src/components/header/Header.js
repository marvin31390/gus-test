import * as React from 'react'
import Title from './Title'
import Logo1 from './Logo1'
import Logo2 from './Logo2'



const mom = {
  display: "flex",
  flexDirection: "column",
  
}
const logo = {
  display: "flex",
  justifyContent: "space-between",

}

const Header = ({data}) => {

  return (
    <div style={mom}>
      <div style={logo}>
        <Logo1 data={data} />
        <Logo2 data={data} />
      </div>
      <div>
      <Title data={data} />
      </div>
    </div>
  )
 }

 export default Header
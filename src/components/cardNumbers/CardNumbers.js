import * as React from "react";

const imgStyle = {
  height: "200px",
  width: "200px",
  margin: "20px",
  borderRadius: "15px",
};

const CardNumbers = ({ card }) => {
  console.log(card);

  return (
    <a href={"tel:" + card.tel}>
      <img style={imgStyle} alt={card.alt} src={card.logo}></img>
    </a>
  );
};

export default CardNumbers;

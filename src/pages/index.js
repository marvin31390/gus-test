import * as React from 'react'
import Header from '../components/header/Header'
import Body from '../components/body/Body'
import Footer from '../components/footer/footer'
import "../style/style.css"
import { graphql } from 'gatsby'


const index = {
  backgroundColor: "#BFBEC5",
  
}


const IndexPage = ({data}) => {
  return (
    <div style={index}>         
        <Header data={data}/>
        <Body data={data} />
        <Footer />
    </div>
  )
}



export const query = graphql`
  query($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  

      allImageSharp {
        edges {
          node {
            gatsbyImageData
          }
        }
      }
      allMarkersJson {
        edges {
          node {
            domicile {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            habiller {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            hebergement {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            infos {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            laver {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            manger {
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
            soigner{
              Nom
              Adresse
              Horaires
              Longitude
              Latitude
              Telephone1
              Web
            }
          }
        }
  }}

 `

export default IndexPage

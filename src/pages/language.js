import * as React from "react";
import Header from "../components/header/Header";
import Footer from "../components/footer/footer";
import Flags from "../components/body/Flags";
import { graphql } from "gatsby";
import "../style/style.css"

const index = {
    backgroundColor: "#BFBEC5",
  };


const Language = ({ data }) => {
    return (
        <div style={index}>
            <Header data={data}/>
            <Flags data={data} />
            <Footer />
        </div>
    )
}

export const query = graphql`
query($language: String!) {
  locales: allLocale(filter: {language: {eq: $language}}) {
    edges {
      node {
        ns
        data
        language
      }
    }
  }
    allImageSharp {
      edges {
        node {
          gatsbyImageData
        }
      }
    }
  }
`;

export default Language
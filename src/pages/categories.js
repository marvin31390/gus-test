import * as React from "react";
import Header from "../components/header/Header";
import {useTranslation} from 'gatsby-plugin-react-i18next';
import Footer from "../components/footer/footer";
import { graphql } from "gatsby";
import "../style/style.css"
import Tel from "../assets/tel.svg"
import Info from "../assets/info.svg"
import Map from "../assets/map.svg"
import Home from "../assets/home.svg"
import Health from "../assets/health.svg"
import Shower from "../assets/shower.svg"
import Fork from "../assets/fork.svg"
import Tshirt from "../assets/tshirt.svg"
import Slumber from "../assets/slumber.svg"

const index = {
    backgroundColor: "#BFBEC5",
  };
  const categories = {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    marginTop: "20px",
  };
  const category = {
    backgroundColor: "#FFF",
    display: "flex",
    justifyContent: "center",
    height: "70px",
    width: "900px",
    textAlign: "center",
    borderRadius: "35px",
    marginLeft: "20px",
    marginRight: "20px",
    marginTop: "20px",
  };
  const textCategory = {
      fontSize: "1.5em",
      fontWeight: "bold",
      margin: "0",
      marginTop: "20px"
      
  };
  const link = {
    textDecoration: "none",
    color: "#000"
  }
  const loli = {
    height: "30px",
    width: "40px",
    marginTop: "6%",
    paddingRight: "3%"
    
    
  };


const Categories = ({ data }) => {
  const {t} = useTranslation();

    return (
        <div style={index}>
            <Header data={data}/>
            <div style={categories}>
        <div style={category}>
          <Tel style={loli}/><a style={link} href="/usefullNumber"><p style={textCategory}>{t('numbers')}</p></a>
        </div>
        <div style={category}>
          <Info style={loli}/><a style={link} href="/map#info"><p style={textCategory}>{t('welcome')}</p></a>
        </div>
        <div style={category}>
          <Map style={loli}/><a style={link} href="/map"><p style={textCategory}>{t('map')}</p></a>
        </div>
        <div style={category}>
          <Home style={loli}/><a style={link} href="/map#domicile"><p style={textCategory}>{t('home')}</p></a>
        </div>

        <div style={category}>
          <Health style={loli}/><a style={link} href="/map#soigner"><p style={textCategory}>{t('heal')}</p></a>
        </div>
        <div style={category}>
          <Shower style={loli}/><a style={link} href="/map#laver"><p style={textCategory}>{t('wash')}</p></a>
        </div>
        <div style={category}>
          <Fork style={loli}/><a style={link} href="/map#manger"><p style={textCategory}>{t('eat')}</p></a>
        </div>
        <div style={category}>
          <Tshirt style={loli}/><a style={link} href="/map#habiller"><p style={textCategory}>{t('dress')}</p></a>
        </div>
        <div style={category}>
          <Slumber style={loli}/><a style={link} href="/map#hebergement"><p style={textCategory}>{t('accomodation')}</p></a>
        </div>
        </div>
            <Footer />
        </div>
    )
}

export const query = graphql`
query($language: String!) {
  locales: allLocale(filter: {language: {eq: $language}}) {
    edges {
      node {
        ns
        data
        language
      }
    }
  }
    allImageSharp {
      edges {
        node {
          gatsbyImageData
        }
      }
    }
   }
`;

export default Categories
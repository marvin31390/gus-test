import * as React from "react";
import Header from "../components/header/Header";
import Footer from "../components/footer/footer";
import ReactMap from "../components/body/ReactMap";
import { graphql } from "gatsby";
import {useTranslation} from 'gatsby-plugin-react-i18next';
import Info from "../assets/info.svg"
import MapIcon from "../assets/map.svg"
import Home from "../assets/home.svg"
import Health from "../assets/health.svg"
import Shower from "../assets/shower.svg"
import Fork from "../assets/fork.svg"
import Tshirt from "../assets/tshirt.svg"
import Slumber from "../assets/slumber.svg"
import "../style/style.css";

const map = {
  height: "505px",
  width: "85%",
  marginLeft: "25px",
  marginTop: "5px",
  borderRadius: "20px",
};
const iconCategories = {
  width: "30px",
  height: "30px",
  marginLeft: "3%",
  flexWrap: "wrap",
  marginTop: "5px",
  backgroundColor: "#FFF",
  borderRadius: "40px",
};
const loli = {
  height: "20px",
  width: "30px",
  marginTop: "15%"
};

const index = {
  backgroundColor: "#BFBEC5",
}
const textUnder = {
  height: "auto",
  width: "230px",
  marginLeft: "20px",
  paddingBottom: '15px',
  paddingTop: "15px",
}
const link = {
  color: "#000"
}
const icons = {
  display: "flex"
}
const Map = ({ data }) => {
  const {t} = useTranslation();
  
  return (

    <div style={index}>
    
      <Header data={data} />
      
      <div style={icons}>
      <div style={iconCategories}>
          <a href="# " style={link} aria-label={t('map')}><MapIcon style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#infos" style={link} aria-label={t('welcome')}><Info style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#domicile" style={link} aria-label={t('home')}><Home style={loli} /></a>
        </div>
        <div style={iconCategories}>
          <a href="#soigner" style={link} aria-label={t('heal')}><Health style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#laver" style={link} aria-label={t('wash')}><Shower style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#manger" style={link} aria-label={t('eat')}><Fork style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#habiller" style={link} aria-label={t('dress')}><Tshirt style={loli}/></a>
        </div>
        <div style={iconCategories}>
          <a href="#hebergement" style={link} aria-label={t('accomodation')}><Slumber style={loli}/></a>
        </div>
      </div>
      <div style={map}>
        <ReactMap data={data}/>
      </div>
      <div style={textUnder}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae
          est id metus dignissim convallis sed et purus. Ut et ultrices sem, ac
          rutrum lacus. Ut pulvinar odio nisl, vitae pretium augue pellentesque
          vel. Quisque dictum consequat leo, eget viverra massa facilisis sed.
          Praesent eget odio ipsum. Praesent tempus tortor in dui commodo
          tincidunt. Ut aliquam rhoncus enim, vel commodo orci. Praesent
          ultricies luctus dui, at eleifend velit pulvinar et.Proin id metus
          sagittis, porta urna ornare, placerat purus. Pellentesque habitant
          morbi tristique senectus et netus et malesuada fames ac turpis
          egestas. Phasellus diam ante, dictum eu sagittis laoreet, tristique
          quis ante. Nunc et elit vitae risus bibendum pharetra non sit amet t.
          Donec purus lectus, mollis quis justo ut, molestie bibendum odio.
          Donec et eleifend quam. Proin scelerisque, nisl quis convallis
          vestibulum, orci lacus tempor nulla, eu tristique neque est fringilla
          nunc. Nulla sit amet tempor enim. Praesent ut leo nisi.Duis quis
          semper sapien.
        </p>
      </div>
      <Footer />
    </div>
  );
};

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    allImageSharp {
      edges {
        node {
          gatsbyImageData
        }
      }
    }
    allMarkersJson {
      edges {
        node {
          domicile {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          habiller {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          hebergement {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          infos {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          laver {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          soigner {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }manger {
            Nom
            Adresse
            Horaires
            Longitude
            Latitude
            Telephone1
            Web
          }
          
        }
      }
  }
}
`;

export default Map;

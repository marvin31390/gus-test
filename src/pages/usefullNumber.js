import * as React from "react";
import Header from "../components/header/Header";
import Footer from "../components/footer/footer";
import CardNumbers from "../components/cardNumbers/CardNumbers";
import { graphql } from "gatsby";
import "../style/style.css"


const index = {
  backgroundColor: "#BFBEC5",
};
const cardsStyle = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: "15px",
  marginBottom: "15px"
};

const UsefullNumber = ({ data }) => {
  let card = data.allUsefullNumberJson.edges[0].node.numbers;

  return (
    <div style={index}>
      <Header data={data} />
      <div style={cardsStyle}>
        {" "}
        {card.length &&
          card.map((card, index) => {
            return <CardNumbers card={card} key={index} />;
          })}
      </div>
      <Footer />
    </div>
  );
};
export const query = graphql`
query($language: String!) {
  locales: allLocale(filter: {language: {eq: $language}}) {
    edges {
      node {
        ns
        data
        language
      }
    }
  }
    allUsefullNumberJson {
      edges {
        node {
          numbers {
            logo
            tel
            alt
          }
        }
      }
    }
    allImageSharp {
      edges {
        node {
          gatsbyImageData
        }
      }
    }
  }
`;
export default UsefullNumber;
